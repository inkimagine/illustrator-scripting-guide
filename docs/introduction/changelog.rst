.. highlight:: javascript
.. _introduction/changelog:

Changelog
#########

What's new and changed for scripting?

----

.. _Changelog.13-0:

`Illustrator XX.X (CC 2017) <>`_
********************************************************************************

- Added: :ref:`jsobjref/Application.getIsFileOpen`

----

.. _Changelog.12-0:

`Illustrator XX.X (CC) <>`_
********************************************************************************


- ?
